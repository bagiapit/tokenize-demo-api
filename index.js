const app = require("express")();
const cors = require("cors");
const WebSocket = require("ws");

const port = 9000;
const webSocketUrl = "wss://stream.binance.com:9443/ws/ethbtc@depth10@1000ms";

app.use(
	cors({
		origin: "*",
	})
);

let maxSize = 150;
let maxAmount = 5;

const filterAndSendDataToClient = (data) => {
  let totalOfAmount = 0;
	const bids = data["bids"].filter((item) => {
		const amounts = totalOfAmount + parseFloat(item[0]) * parseFloat(item[1]);
		if (amounts < maxAmount) {
			totalOfAmount = amounts;
			return true;
		}
		return false;
	});
	let totalOfSize = 0;
	const asks = data["asks"].filter((item) => {
		const sizes = totalOfSize + parseFloat(item[1]);
		if (sizes <= maxSize) {
			totalOfSize = sizes;
			return true;
		}
		return false;
	});
	socket.emit("notification", {
		data: {
			bids,
			asks,
			totalOfAmount,
			totalOfSize,
		},
	});
}

const wss = new WebSocket(webSocketUrl);
wss.onmessage = (msg) => {
	const data = JSON.parse(msg.data);
	filterAndSendDataToClient(data);
};

const server = app.listen(port, () => {
	console.log(`Tokenize API listing on port ${port}`);
});

const socket = require("socket.io")(server, {
	cors: {
		origin: "*",
	},
});

socket.on("connection", (socket) => {
	console.log("Socket: client connected");
	socket.on("notification", (mes) => {
		console.log("Message from client with ♥:", mes);
	});
});

app.get("/", (req, res) => {
	res.send("Tokenize API");
});
